﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPistol : MonoBehaviour
{
    
    public Transform GunBarrel; //end of gun barrel
    float Delay;
    public float HurtingExplosion = 100f;
    public float HurtingEnemy = 40f;
    public int FiringSpeed = 50; // this is how far the bullet travels
    public float destroybullet = 3.0f; 
    public bool AbleToFire = false; // 
    public float DelayBullet = 2000f; // this makes the bullets wait a bit before firing again
    RaycastHit ObjectsHitted; //this tells you what you hit after what bellow this figures out what you hit
    Ray FireObject;
    public Transform Player;
    public float PistolAmmoDecresed = 1;
   // PistolAmmo HandGunAmmo;
    void Start()
    {
      //  HandGunAmmo = GetComponent<PistolAmmo>();
    }

    public  void Update()
    {
       

        if (Input.GetButtonDown("Fire1")) //if buton to fire gun is pressed check if able to fire is true 

        {
            if (AbleToFire == true) //if able to fire is true then run the shoot function and the delayplayerbullet function
            {
              
                Shoot(); //fires gun
                StartCoroutine(DelayPlayerbullet());
                AbleToFire = false;

                Player.GetComponent<PistolAmmo>().AmmoReduced(PistolAmmoDecresed);

            }
        }
    }
    IEnumerator DelayPlayerbullet()
    {
        yield return new WaitForSeconds(DelayBullet); //wait for seconds is a code that makes things wait after you perform a action for what ever time you set
       
        AbleToFire = true;

    }


    void Shoot()
    {

      

        if (Physics.Raycast(GunBarrel.transform.position, GunBarrel.transform.forward, out ObjectsHitted, FiringSpeed)) //Fires the raycast forward of the barrel position and returns what ever object it hit
        {                      
         
            if (ObjectsHitted.transform.CompareTag ("Arm")) //if the object hitted tag is arm and the parent object has a script called enemy health reduce health
            {
                Debug.Log("arm hit");
                EnemyHealth enemyhealther = ObjectsHitted.transform.parent.gameObject.GetComponent<EnemyHealth>();
                if (enemyhealther == null)
                    return;
                enemyhealther.EnemyDamaged(HurtingEnemy);
            }


            if (ObjectsHitted.transform.CompareTag("Body")) //if the object hitted tag is body and the parent object has a script called enemy health reduce twice as much health as hurt enemy holds

            {
                Debug.Log("arm hit");
                EnemyHealth enemyhealther = ObjectsHitted.transform.parent.gameObject.GetComponent<EnemyHealth>();
                if (enemyhealther == null)
                    return;
                enemyhealther.EnemyDamaged(HurtingEnemy * 2);
            }


           

        }
        else //if you hit somthing else do nothing 
        {
          
            return;
        }
    }
}
