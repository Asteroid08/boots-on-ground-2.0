﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyMovement : MonoBehaviour
{
    // Start is called before the first frame update
    NavMeshAgent NavAgent; // nav agent 
   public Transform Player; //this tells it that the player is the target that the enemy will chase but we havent put it in code yet so it wont chase the player yet
    //make it so when player is in range the tank attacks the player but when player is not in range make the enemy attack the base 
    public float EnemyAttackRange = 31f;
 public    float PlayerwithinRange;
   

    //bool TRIGGERED = false;

    // Use this for initialization
    void Start()
    {
        NavAgent = GetComponent<NavMeshAgent>(); //we get the navmeshagent because we use it later on



    }




    // Update is called once per frame
    void Update()
    {



        PlayerwithinRange = Vector3.Distance(NavAgent.transform.position, Player.position); //This tracks the players position for us 



        if (PlayerwithinRange <= EnemyAttackRange) //this is saying if player is within enemy attack range to pursue it 
        {



            NavAgent.SetDestination(Player.position); ////navAgent is the nav mesh and its just asking where do you want to go we put Player  position and it tells the nav mesh it wants to go towards the player

        }
    }
}
