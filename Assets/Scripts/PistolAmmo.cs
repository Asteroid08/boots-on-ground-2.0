﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PistolAmmo : MonoBehaviour
{
    // Start is called before the first frame update
    public float Ammo = 25f;//start ammo in clip 
    public float TotalAmmo = 100f;
    public float PistolAmmoCap = 30f;
    public ShootPistol shooter;
    public float PistolMaxAmmo = 30f;
    public float RestOfAmmo;

    public Text ClipCounter;
    public Text pouchCounter;
    // Use this for initialization
    void Start()
    {

    }

    public void AmmoReduced(float ammocount) //ammocount receives its numbers from the enemy shooting script
    {
        Ammo -= ammocount; 
  
          

            
        //}
    }
     void Update()
    {
        ClipCounter.text = Ammo.ToString();
        pouchCounter.text = TotalAmmo.ToString();
        if (Input.GetKeyDown(KeyCode.R))
        {

            if (TotalAmmo <= 0)
            {
                return;
            }
            if (TotalAmmo <= 59)
            {
              RestOfAmmo = TotalAmmo + Ammo;
                TotalAmmo = RestOfAmmo - 30;
                //  TotalAmmo -= Ammo;
                Ammo = Mathf.Clamp(RestOfAmmo, 0, PistolMaxAmmo);
                RestOfAmmo = 0;
            }
        

                if (TotalAmmo >= 30)
            {
                 PistolAmmoCap -= Ammo;
                TotalAmmo -= PistolAmmoCap;
                Ammo = TotalAmmo;
                //Ammo = 30;
                //TotalAmmo -= Ammo;

                
                
                Ammo = Mathf.Clamp(Ammo, 0, PistolMaxAmmo);
                //    TotalAmmo -= PistolAmmoCap;
                PistolAmmoCap = 30;
            }
            
            }
           if (TotalAmmo <= 0 && Ammo <= 0)
        {
            shooter.enabled = false;
        }
            }

   

        }
       
    

