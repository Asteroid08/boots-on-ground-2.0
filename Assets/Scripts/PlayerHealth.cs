﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;
public class PlayerHealth : MonoBehaviour
{
    // Start is called before the first frame update
    public float beginningHealth = 100f;//start health 
  public  Movement PlayerMover;
    public Canvas Retry;
    public ShootPistol shooter;
    public PostProcessProfile HealthBar;
    public float HealthBarAmount = 0f;
    
    
    private Vignette Vin;
    // Use this for initialization
    void Start()
    {
        if (HealthBar.HasSettings <Vignette>())
        {
            HealthBar.GetSetting<Vignette>().intensity.value = 0f;
        }
    }
    public void playerHealthBar(float HPamount)
    {

        HealthBarAmount += HPamount;
        HealthBarAmount = HealthBar.GetSetting<Vignette>().intensity.value;

    }
    public void PlayerDamaged(float amount) //amount receives its numbers from the enemy shooting script
    {
        beginningHealth -= amount; //the number amount carrys is subtracted from beginning healhs current health
        
        if (beginningHealth <= 0) //if beginning health is 0 or less disable the players gun the players movement and enable the canvas
        {
            PlayerMover.enabled = false;
            Retry.enabled = true;
            shooter.enabled = false;
            if (HealthBar.HasSettings<Vignette>())
            {
                HealthBar.GetSetting<Vignette>().intensity.value = 0.8f;
            }
        }
    }

    

}

