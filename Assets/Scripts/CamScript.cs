﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform Player; //The players transform
    public Transform Cam; //The cameras transform
    // Start is called before the first frame update

    public float HorizontalSens = 2000; // The Horizontal sensitivity value
    public float VerticalSens = 2000; //The vertical senstivity value
    public float pitch;
    public float Clampview = 70;
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float RotateHoz = Input.GetAxis("Mouse X") * HorizontalSens * Time.deltaTime; //Gives the value of horizontalsens to RotateHoz and gets the X Axis

        Player.Rotate(0, RotateHoz, 0); //Rotates the player object horizontally when they move there mouse horizontally 

        float RotateVer = Input.GetAxis("Mouse Y") * VerticalSens * Time.deltaTime;

    ///////   // Cam.Rotate(RotateVer, 0, 0); //
        Player.Rotate(RotateVer, 0, 0);
        pitch = Mathf.Clamp(pitch + RotateVer, -Clampview, Clampview);

     ////////  // Cam.rotation = Quaternion.Euler (-pitch, Player.rotation.eulerAngles.y, 0);
        Player.rotation = Quaternion.Euler(-pitch, Cam.rotation.eulerAngles.y, 0);

    }
}
