﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyShooting : MonoBehaviour
{
    // Start is called before the first frame update
    NavMeshAgent NavAgent; // nav agent 
    public Transform Player; //The Players Transform
    public float EnemyAttackRange = 31f;
    public float PlayerwithinRange;

    public Transform EnemyGunBarrel; //end of gun barrel
    float Delay;

    public float HurtingEnemy = 40f;
    public int FiringSpeed = 50; // this is how far the bullet will travel  
                               
    public bool AbleToFire = true; // 
    public float DelayBullet = 0.10f; // this makes the bullets wait a bit before firing again 
    RaycastHit ObjectsHitted; //this tells you what you hit after what bellow this figures out what you hit
    Ray FireObject;
    public float HPdamage = 0.1f;

    // Use this for initialization
    void Start()
    {
        NavAgent = GetComponent<NavMeshAgent>(); //we get the navmeshagent because we use it later on



    }




    // Update is called once per frame
    public void Update()
    {
        PlayerwithinRange = Vector3.Distance(NavAgent.transform.position, Player.position); //first we put transform.position transform.positon is the enemy subs current position and we set it to the player by putting sub.position this keeps track of the players position and how far it is from the enemy   

        if (PlayerwithinRange <= EnemyAttackRange) //this is saying if player is within enemy attack range to check if able to fire is true 
        {
            //if able to fire is true play whats in shoot and Ieumerator
            if (AbleToFire) //Able to fire will always be true for the demo but will change in future versions to false 
            {
                AbleToFire = true;
                Shoot(); //fires gun 
                StartCoroutine(DelayPlayerbullet());

            }



        }

        IEnumerator DelayPlayerbullet()
        {
            yield return new WaitForSeconds(DelayBullet); //wait for seconds is a code that makes things wait after you perform a action for what ever time you set
            AbleToFire = true;

        }
        void Shoot()
        {
           
         //Fires a raycast forward of the enemygunbarrel and ObjectsHitted asks what the raycast hit 
            if (Physics.Raycast(EnemyGunBarrel.transform.position, EnemyGunBarrel.transform.forward, out ObjectsHitted, FiringSpeed))
            {                      
               
                if (ObjectsHitted.transform.CompareTag("Player")) //if the object that was hit by the raycast tag is player and the parent object has the PlayerHealth script health will be reduced  
                {
                    Debug.Log("arm hit");
                    PlayerHealth playerhealther = ObjectsHitted.transform.parent.gameObject.GetComponent<PlayerHealth>();
                    if (playerhealther == null)
                        return;
                    playerhealther.PlayerDamaged(HurtingEnemy);
                  //  playerhealther.playerHealthBar(HPdamage);
                }



        
          


            }
            else         //if not do nothing 
            {
                return;
            }
        }
    }
}


    

